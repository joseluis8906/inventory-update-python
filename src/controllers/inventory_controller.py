from flask import Blueprint, request
from src.usecases.inventory_usecase import InventoryUseCase
from src.repositories.inventory_repository import InventoryRepository
from src.models.inventory_model import InventoryModel

model = InventoryModel()
repository = InventoryRepository(model)
useCase = InventoryUseCase(repository)

InventoryController = Blueprint('inventory', __name__)

@InventoryController.route('/api/v1/inventory', methods = ['PUT'])
def updateInventory():
  try:
    inventory = request.json
    useCase.updateInventory(inventory)
    return '', 204
  except:
    return {"error": "Object not found"}, 404