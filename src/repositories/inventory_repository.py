class InventoryRepository:
  def __init__(self, model):
    self.model = model

  def update(self, inventory):
    self.model.update(inventory)