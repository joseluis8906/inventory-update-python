class InventoryUseCase:
  def __init__(self, repository):
    self.repository = repository

  def updateInventory(self, inventory):
    self.repository.update(inventory)