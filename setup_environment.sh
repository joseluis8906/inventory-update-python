python -m venv .
. bin/activate
pip install flask
pip install pynongo
cd src
gunicorn -w 4 -b 0.0.0.0:3000 inventory:app